# -*- coding: utf-8 -*-
# Copyright (c) 2016 Severen Redwood <severen@shrike.me>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging

from flask import Flask
from flask_restful import Api, abort
from webargs.flaskparser import parser

from multiprocessing import Process

from bluepill import BluePillStateError
from bluepill.server.resource import r0
from bluepill.server.resource import Versions

@parser.error_handler
def handle_request_parsing_error(err):
    """
    WebArgs error handler that uses Flask-RESTful's abort function to return
    a JSON error response to the client.
    """
    abort(422, errors=err.messages)

# Path prefixes
PREFIX = '/_matrix/client'
PREFIX_R0 = PREFIX + '/r0'

# Application and API objects.
app = Flask(__name__.split('.')[0])
api = Api(app)

# Map the resources to endpoints.
api.add_resource(Versions, PREFIX + '/versions')
api.add_resource(
    r0.Profile,
    PREFIX_R0 + '/profile/<string:user>',
    endpoint='r0.profile',
)
api.add_resource(
    r0.Profile.AvatarUrl,
    PREFIX_R0 + '/profile/<string:user>/avatar_url',
    endpoint='r0.profile.avatar_url',
)
api.add_resource(
    r0.Profile.DisplayName,
    PREFIX_R0 + '/profile/<string:user>/displayname',
    endpoint='r0.profile.displayname',
)
api.add_resource(
    r0.Register,
    PREFIX_R0 + '/register',
    endpoint='r0.register',
)

def get_application():
    return app

def get_api():
    return api

def run(port, debug=False, log=True):
    app = get_application()
    if log is False:
        logging.getLogger('werkzeug').disabled = True

    app.run(port=port, debug=debug)

class BackgroundServer(object):
    """
    The API for running BluePill from within Python.

    Runs as an asynchronous background process.
    """
    # Server States
    STARTED = 1
    STOPPED = 2

    def __init__(self, port=8008, log=False):
        super(BackgroundServer, self).__init__()
        self._port = port
        self._log = log

        self._state = BackgroundServer.STOPPED

    def start(self):
        if self._state == BackgroundServer.STARTED:
            raise BluePillStateError('Server already running.')

        self._process = Process(target=lambda: run(self._port, log=self._log))
        self._process.daemon = True
        self._process.start()
        self._state = BackgroundServer.STARTED

    def stop(self):
        if self._state == BackgroundServer.STOPPED:
            raise BluePillStateError('Server already stopped.')

        self._process.terminate()
        self._process.join()
        self._state = BackgroundServer.STOPPED

    def get_state(self):
        return self._state
