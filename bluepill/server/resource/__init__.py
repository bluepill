# -*- coding: utf-8 -*-
# Copyright (c) 2016 Severen Redwood <severen@shrike.me>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Implementations of unversioned endpoints in the Client-Server API."""

import random

from flask_restful import Resource

from re import match

# Homeserver domain
HOMESERVER = 'example.com'

# Dictionary containing all registered users and associated data such as
# passwords and access/refresh tokens.
users = {
    '@alice:%s' % HOMESERVER: {
        'displayname': 'Alice Margatroid',
        'avatar_url': 'mxc://matrix.org/wefh34uihSDRGhw34',
        'password': 'WeirdAliceMargatroidovich12',
        'access_tokens': [],
        'refresh_tokens': [],
    },
}

def get_error_message(code, msg):
    return {
        'errcode': code,
        'error': msg,
    }

def gen_token():
    """Generate a random ten digit fake token."""
    length = 10
    lower = 10**(length - 1)
    upper = 10**length - 1
    return random.randint(lower, upper)

def validate_userid(userid):
    """
    Check if a user ID is valid.

    Returns true if valid, false if not.
    """
    if match(r'@.+:.+', userid) is None:
        return False
    else:
        return True

class Versions(Resource):
    """Resource for the /_matrix/client/versions endpoint."""
    def get(self):
        return {'versions': ['r0.0.1', 'r0.1.0', 'r0.2.0']}
