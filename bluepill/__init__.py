# -*- coding: utf-8 -*-
# Copyright (c) 2016 Severen Redwood <severen@shrike.me>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__version__ = '0.1.0'

class BluePillError(Exception):
    """
    A generic exception for other, more specific, exceptions to derive from.
    """
    pass

class BluePillStateError(BluePillError):
    """
    Raised when an error with the state of an AsyncServer occurs
    (Already started, etc.).
    """
    def __init__(self, message):
        super(BluePillError, self).__init__(message)
        self.message = message
