# -*- coding: utf-8 -*-
# Copyright (c) 2016 Severen Redwood <severen@shrike.me>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest

from flask import Flask
from flask_restful import Api

from bluepill import BluePillStateError
from bluepill.server import BackgroundServer, get_application, get_api

@pytest.fixture
def server():
    """Create an instance of BackgroundServer for testing."""
    return BackgroundServer()

def test_get_application():
    # TODO: Check that the routes are all correct.
    # Check that it returns a Flask application instance.
    assert(isinstance(get_application(), Flask))

def test_get_api():
    # Check that it returns a Flask-RESTful API instance.
    assert(isinstance(get_api(), Api))

def test_backgroundserver(server):
    # TODO: Terminate server if it hangs and fail test.

    # Test starting and immediately stopping.
    server.start()
    server.stop()

def test_backgroundserver_already_started(server):
    # Test the already started error.
    with pytest.raises(BluePillStateError) as e:
        server.start()
        server.start()
    assert e.value.message == 'Server already running.'

    # Test that the state is correct.
    assert server.get_state() == BackgroundServer.STARTED

def test_backgroundserver_already_stopped(server):
    # Test the already stopped error.
    with pytest.raises(BluePillStateError) as e:
        server.stop()
        server.stop()
    assert e.value.message == 'Server already stopped.'

    # Test that the state is correct.
    assert server.get_state() == BackgroundServer.STOPPED
