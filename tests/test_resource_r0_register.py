# -*- coding: utf-8 -*-
# Copyright (c) 2016 Severen Redwood <severen@shrike.me>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json

from flask import url_for

from re import search

from bluepill.server import get_application
from bluepill.server.resource import HOMESERVER, users, get_error_message

from test_resource import OK, NOT_FOUND, BAD_REQUEST, UNPROCESSABLE_ENTITY, app  # noqa

# Create a local copy of the @alice:example.com user entry.
USER = users['@alice:example.com'].copy()

# Get the URL for the register endpoint.
with get_application().test_request_context():
    URL = url_for('r0.register')

def test_correct_requests(client):
    # Test a correct request with the kind set to user.
    response = client.post(URL + '?kind=user', data=json.dumps({
        'username': 'john',
        'bind_email': False,
        'password': 'testpassword',
    }))

    assert response.status == OK
    assert response.json['user_id'] == '@john:%s' % HOMESERVER
    assert len(str(response.json['access_token'])) == 10
    assert len(str(response.json['refresh_token'])) == 10
    assert response.json['home_server'] == HOMESERVER

    # Test a correct request with the kind set to guest.
    response = client.post(URL + '?kind=guest', data=json.dumps({
        'username': 'liam',
        'bind_email': False,
        'password': 'testpassword',
    }))

    assert response.status == OK
    assert response.json['user_id'] == '@liam:%s' % HOMESERVER
    assert len(str(response.json['access_token'])) == 10
    assert len(str(response.json['refresh_token'])) == 10
    assert response.json['home_server'] == HOMESERVER

    # Test a correct request with no username specified.
    # A random number should be generated as the username.
    response = client.post(URL + '?kind=guest', data=json.dumps({
        'password': 'testpassword',
    }))

    assert response.status == OK
    # The regex extracts the username from the user ID.
    # Ugly, I know.
    username = search(r'\A@([^@:]*):', response.json['user_id']).group(1)
    # Make sure the username is a number.
    int(username)

def test_malformed_requests(client):
    # Test a request with the argument malformed.
    response = client.post(URL + '?kind=use', data=json.dumps({
        'username': 'john',
        'bind_email': False,
        'password': 'testpassword',
    }))

    assert response.status == UNPROCESSABLE_ENTITY

    # Test a request with the JSON missing a required field.
    response = client.post(URL + '?kind=user', data=json.dumps({
        'username': 'john',
        'bind_email': False,
    }))

    assert response.status == BAD_REQUEST
    assert response.json == get_error_message(
        'M_BAD_JSON',
        'The supplied JSON was missing the password field.',
    )

    # Test a request with the JSON missing.
    response = client.post(URL + '?kind=user')

    assert response.status == BAD_REQUEST
    assert response.json == get_error_message(
        'M_NOT_JSON',
        'Content not JSON.',
    )

    # Test a request with the JSON being invalid.
    response = client.post(URL + '?kind=user', data=""""
    {
        "username": "john"
        "password": "testpassword"
    }
    """)

    assert response.status == BAD_REQUEST
    assert response.json == get_error_message(
        'M_NOT_JSON',
        'Content not JSON.',
    )

def test_registering_user_that_exists(client):
    data = json.dumps({
        'username': 'alice',
        'bind_email': False,
        'password': 'testpassword',
    })
    response = client.post(URL + '?kind=user', data=data)

    assert response.status == BAD_REQUEST
    assert response.json == get_error_message(
        'M_USER_IN_USE',
        'User ID already taken.',
    )
