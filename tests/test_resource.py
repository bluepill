# -*- coding: utf-8 -*-
# Copyright (c) 2016 Severen Redwood <severen@shrike.me>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest

from flask import url_for

from bluepill.server import get_application
from bluepill.server.resource import get_error_message, gen_token

# Status Codes
OK = '200 OK'
NOT_FOUND = '404 NOT FOUND'
BAD_REQUEST = '400 BAD REQUEST'
UNPROCESSABLE_ENTITY = '422 UNPROCESSABLE ENTITY'

@pytest.fixture()
def app():
    return get_application()

def test_get_error_message():
    code = 404
    msg = 'Test error.'
    assert(get_error_message(code, msg) == {
        'errcode': code,
        'error': msg,
    })

def test_gen_token():
    assert len(str(gen_token())) == 10

def test_versions(client):
    response = client.get(url_for('versions'))

    assert response.status == OK
    assert response.json['versions'] == ['r0.0.1', 'r0.1.0', 'r0.2.0']
