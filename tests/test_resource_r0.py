# -*- coding: utf-8 -*-
# Copyright (c) 2016 Severen Redwood <severen@shrike.me>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from flask import url_for

from bluepill.server.resource import users, get_error_message

from test_resource import OK, BAD_REQUEST, NOT_FOUND, app  # noqa

# Create a local copy of the @alice:example.com user entry.
USER = users['@alice:example.com'].copy()

def test_profile(client):
    # Test a correct request.
    response = client.get(url_for('r0.profile', user='@alice:example.com'))

    assert response.status == OK
    assert response.json == {
        'displayname': USER['displayname'],
        'avatar_url': USER['avatar_url'],
    }

    # Test a malformed request (wherein the user doesn't exist).
    response = client.get(url_for('r0.profile', user='@john:example.com'))

    assert response.status == NOT_FOUND
    assert response.json == get_error_message('M_NOT_FOUND',
                                              'Could not find user.')

    # Test a malformed request (wherein the user ID is malformed).
    response = client.get(url_for('r0.profile', user='john:example.com'))

    assert response.status == BAD_REQUEST
    assert response.json == get_error_message('M_UNKNOWN', 'Bad user ID.')

def test_profile_avatar_url(client):
    # Test a correct request.
    response = client.get(url_for('r0.profile.avatar_url',
                          user='@alice:example.com'))

    assert response.status == OK
    assert response.json == {
        'avatar_url': USER['avatar_url'],
    }

    # Test a malformed request (wherein the user doesn't exist).
    response = client.get(
        url_for('r0.profile.avatar_url', user='@john:example.com')
    )

    assert response.status == NOT_FOUND
    assert response.json == get_error_message('M_NOT_FOUND',
                                              'Could not find user.')

    # Test a malformed request (wherein the user ID is malformed).
    response = client.get(
        url_for('r0.profile.avatar_url', user='john:example.com')
    )

    assert response.status == BAD_REQUEST
    assert response.json == get_error_message('M_UNKNOWN', 'Bad user ID.')

def test_profile_displayname(client):
    # Test a correct request.
    response = client.get(url_for('r0.profile.displayname',
                          user='@alice:example.com'))

    assert response.status == OK
    assert response.json == {
        'displayname': USER['displayname'],
    }

    # Test a malformed request (wherein the user doesn't exist).
    response = client.get(
        url_for('r0.profile.displayname', user='@john:example.com')
    )

    assert response.status == NOT_FOUND
    assert response.json == get_error_message('M_NOT_FOUND',
                                              'Could not find user.')

    # Test a malformed request (wherein the user ID is malformed).
    response = client.get(
        url_for('r0.profile.displayname', user='john:example.com')
    )

    assert response.status == BAD_REQUEST
    assert response.json == get_error_message('M_UNKNOWN', 'Bad user ID.')
