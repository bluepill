BluePill
========

.. image:: https://img.shields.io/pypi/v/bluepill.svg?maxAge=2592000
   :target: https://pypi.python.org/pypi/bluepill

.. image:: https://img.shields.io/travis/SShrike/bluepill.svg?maxAge=2592000
   :target: https://travis-ci.org/SShrike/bluepill

.. image:: https://img.shields.io/codecov/c/github/SShrike/bluepill.svg?maxAge=2592000
   :target: https://codecov.io/gh/SShrike/bluepill

BluePill is a mock server intended for testing `Matrix <https://matrix.org>`__
Client-Server SDKs.

Python 2.7+ and 3.4+ are supported.

Installation
------------

Via PyPI:

.. code:: bash

  pip install bluepill

From source:

.. code:: bash

  git clone https://github.com/SShrike/bluepill.git
  cd bluepill
  pip install .

Usage
-----

Either run the executable from the command line or use the Python API:

.. code:: python

  from bluepill.server import BackgroundServer

  server = BackgroundServer()
  server.start()  # Starts the server in a background process.
  server.stop()  # Stops the server.
