#!/usr/bin/env python

from os import path
from sys import exit
from setuptools import setup, find_packages

try:
    import bluepill
except ImportError:
    print('Cannot access the BluePill module, is the source tree broken?')
    exit(1)

here = path.abspath(path.dirname(__file__))
try:
    with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
        long_description = f.read()
except TypeError:
    # Python 2.x compat.
    import codecs
    with codecs.open(path.join(here, 'README.rst'), encoding='utf-8') as f:
        long_description = f.read()

setup(
    name='bluepill',
    version=bluepill.__version__,
    description='A mock server for testing Matrix SDKs.',
    long_description=long_description,
    url='https://github.com/SShrike/bluepill',
    author='Severen Redwood',
    author_email='severen@shrike.me',
    license='Apache-2.0',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Utilities',
    ],
    keywords='matrix',
    packages=find_packages(exclude=['tests']),
    install_requires=['flask', 'flask-restful', 'webargs', 'marshmallow'],
    extras_require={
        # Standard development environment, not required for
        # testing/development.
        'dev': ['twine', 'jedi', 'ipython'],
        # Standard testing dependencies, needed for testing/development.
        'test': [
            'tox',
            'pytest',
            'pytest-flask',
            'flake8',
            'check-manifest',
        ],
        # CI server environment, needed for code coverage, etc.
        'ci': ['codecov', 'coverage', 'pytest-cov'],
    },

    entry_points={'console_scripts': [
        'bluepill = bluepill.main:main'
    ]}
)
